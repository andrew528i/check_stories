package config

const (
	WATCHER_CHECKING_TIMEOUT       = 60 // max seconds for profile being in checking status wo changes
	WATCHER_STORY_CHECKING_TIMEOUT = 20 // seconds too

	WATCHER_STORY_CHECK_BATCH_SIZE = 5
)
