pb:
	@echo "[ \033[32mbackend proto\033[0m ]"
	protoc --proto_path=./proto --go_out=plugins=grpc:./service/backend/grpc_handler/pb ./proto/backend*.proto

	@echo "[ \033[32mtelegram proto\033[0m ]"
	protoc --proto_path=./proto --go_out=plugins=grpc:./service/telegram/grpc_handler/pb ./proto/telegram*.proto

models:
	@echo "[ \033[32ment models\033[0m ]"
	entc generate ./ent/schema
