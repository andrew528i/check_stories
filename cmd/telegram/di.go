package main

import (
	"story_check/service/common"
	"story_check/service/common/di"
	"story_check/service/telegram"
	"story_check/service/telegram/grpc_handler"
	"story_check/service/telegram/telegram_handler"
)

func initDependencies() {
	di.InitContainer()

	di.Provide(common.NewBackendClient)
	di.Provide(telegram.NewBot)
	di.Provide(telegram_handler.NewTelegramHandler)
	di.Provide(grpc_handler.NewTelegramAPIHandler)
}
