package main

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"story_check/config"
	"story_check/service/common/di"
	"story_check/service/telegram/grpc_handler"
	telegram "story_check/service/telegram/grpc_handler/pb"
	"story_check/service/telegram/telegram_handler"
)

func main() {
	initDependencies()

	var telegramHandler *telegram_handler.TelegramHandler
	var grpcHandler *grpc_handler.TelegramAPIHandler

	di.Invoke(func(th *telegram_handler.TelegramHandler, g *grpc_handler.TelegramAPIHandler) {
		telegramHandler = th
		grpcHandler = g
	})

	go telegramHandler.Start()

	address := fmt.Sprintf("0.0.0.0:%d", config.TELEGRAM_PORT)
	lis, err := net.Listen("tcp", address)
	if err != nil {
		panic(err)
	}

	server := grpc.NewServer()

	di.Invoke(func(apiHandler *grpc_handler.TelegramAPIHandler) {
		telegram.RegisterTelegramAPIServiceServer(server, apiHandler)
	})

	reflection.Register(server)

	if err = server.Serve(lis); err != nil {
		panic(err)
	}
}
