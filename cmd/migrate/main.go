package main

import (
	"context"

	"story_check/ent"
	"story_check/ent/migrate"
	"story_check/service/common/di"
)

func main() {
	di.InitContainer()

	di.Invoke(func(client *ent.Client) {
		defer client.Close()

		if err := client.Schema.Create(
			context.Background(),
			migrate.WithDropColumn(true),
		); err != nil {
			panic(err)
		}
	})
}
