package main

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"story_check/config"
	"story_check/service/backend/grpc_handler"
	backend "story_check/service/backend/grpc_handler/pb"
	"story_check/service/common/di"
)

func main() {
	initDependencies()

	address := fmt.Sprintf("0.0.0.0:%d", config.BACKEND_PORT)
	lis, err := net.Listen("tcp", address)
	if err != nil {
		panic(err)
	}

	server := grpc.NewServer()

	di.Invoke(func(apiHandler *grpc_handler.APIHandler) {
		backend.RegisterAPIServiceServer(server, apiHandler)
	})

	reflection.Register(server)

	if err = server.Serve(lis); err != nil {
		panic(err)
	}
}
