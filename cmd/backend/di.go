package main

import (
	"story_check/service/backend/grpc_handler"
	"story_check/service/common/di"
)

func initDependencies() {
	di.InitContainer()

	di.Provide(grpc_handler.NewAPIHandler)
}
