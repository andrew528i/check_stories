package main

import (
	"story_check/service/common"
	"story_check/service/common/di"
	"story_check/service/watcher"
)

func initDependencies() {
	di.InitContainer()

	di.Provide(watcher.NewAccountWatcher)
	di.Provide(watcher.NewNotificationWatcher)
	di.Provide(watcher.NewProfileWatcher)
	di.Provide(common.NewTelegramClient)
}
