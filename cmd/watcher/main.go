package main

import (
	"story_check/service/common/di"
	"story_check/service/watcher"
)

func main() {
	initDependencies()

	di.Invoke(func(profileWatcher *watcher.ProfileWatcher, notificationWatcher *watcher.NotificationWatcher) {
		go profileWatcher.RunCheckProfiles()
		go profileWatcher.RunCheckProfileStories()

		go notificationWatcher.Run()
	})

	select {}
}
