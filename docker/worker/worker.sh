#!/usr/bin/env bash

export PYTHONPATH=/app
python3.7 /app/service/worker/app_profile.py&
python3.7 /app/service/worker/app_story.py&

sleep 180 && sudo kill -s SIGTERM 1
