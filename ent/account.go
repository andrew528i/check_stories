// Code generated by entc, DO NOT EDIT.

package ent

import (
	"fmt"
	"story_check/ent/account"
	"strings"
	"time"

	"github.com/facebookincubator/ent/dialect/sql"
)

// Account is the model entity for the Account schema.
type Account struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// Username holds the value of the "username" field.
	Username string `json:"username,omitempty"`
	// Password holds the value of the "password" field.
	Password string `json:"password,omitempty"`
	// Cookies holds the value of the "cookies" field.
	Cookies []byte `json:"cookies,omitempty"`
	// LastActivity holds the value of the "last_activity" field.
	LastActivity time.Time `json:"last_activity,omitempty"`
	// Status holds the value of the "status" field.
	Status account.Status `json:"status,omitempty"`
}

// FromRows scans the sql response data into Account.
func (a *Account) FromRows(rows *sql.Rows) error {
	var va struct {
		ID           int
		Username     sql.NullString
		Password     sql.NullString
		Cookies      []byte
		LastActivity sql.NullTime
		Status       sql.NullString
	}
	// the order here should be the same as in the `account.Columns`.
	if err := rows.Scan(
		&va.ID,
		&va.Username,
		&va.Password,
		&va.Cookies,
		&va.LastActivity,
		&va.Status,
	); err != nil {
		return err
	}
	a.ID = va.ID
	a.Username = va.Username.String
	a.Password = va.Password.String
	a.Cookies = va.Cookies
	a.LastActivity = va.LastActivity.Time
	a.Status = account.Status(va.Status.String)
	return nil
}

// QuerySocialNetwork queries the social_network edge of the Account.
func (a *Account) QuerySocialNetwork() *SocialNetworkQuery {
	return (&AccountClient{a.config}).QuerySocialNetwork(a)
}

// QueryCheckedContent queries the checked_content edge of the Account.
func (a *Account) QueryCheckedContent() *ContentQuery {
	return (&AccountClient{a.config}).QueryCheckedContent(a)
}

// Update returns a builder for updating this Account.
// Note that, you need to call Account.Unwrap() before calling this method, if this Account
// was returned from a transaction, and the transaction was committed or rolled back.
func (a *Account) Update() *AccountUpdateOne {
	return (&AccountClient{a.config}).UpdateOne(a)
}

// Unwrap unwraps the entity that was returned from a transaction after it was closed,
// so that all next queries will be executed through the driver which created the transaction.
func (a *Account) Unwrap() *Account {
	tx, ok := a.config.driver.(*txDriver)
	if !ok {
		panic("ent: Account is not a transactional entity")
	}
	a.config.driver = tx.drv
	return a
}

// String implements the fmt.Stringer.
func (a *Account) String() string {
	var builder strings.Builder
	builder.WriteString("Account(")
	builder.WriteString(fmt.Sprintf("id=%v", a.ID))
	builder.WriteString(", username=")
	builder.WriteString(a.Username)
	builder.WriteString(", password=")
	builder.WriteString(a.Password)
	builder.WriteString(", cookies=")
	builder.WriteString(fmt.Sprintf("%v", a.Cookies))
	builder.WriteString(", last_activity=")
	builder.WriteString(a.LastActivity.Format(time.ANSIC))
	builder.WriteString(", status=")
	builder.WriteString(fmt.Sprintf("%v", a.Status))
	builder.WriteByte(')')
	return builder.String()
}

// Accounts is a parsable slice of Account.
type Accounts []*Account

// FromRows scans the sql response data into Accounts.
func (a *Accounts) FromRows(rows *sql.Rows) error {
	for rows.Next() {
		va := &Account{}
		if err := va.FromRows(rows); err != nil {
			return err
		}
		*a = append(*a, va)
	}
	return nil
}

func (a Accounts) config(cfg config) {
	for _i := range a {
		a[_i].config = cfg
	}
}
