// Code generated by entc, DO NOT EDIT.

package ent

import (
	"fmt"
	"strings"

	"github.com/facebookincubator/ent/dialect/sql"
)

// SocialNetwork is the model entity for the SocialNetwork schema.
type SocialNetwork struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// Name holds the value of the "name" field.
	Name string `json:"name,omitempty"`
	// APIURL holds the value of the "api_url" field.
	APIURL string `json:"api_url,omitempty"`
	// URL holds the value of the "url" field.
	URL string `json:"url,omitempty"`
	// IsAlive holds the value of the "is_alive" field.
	IsAlive bool `json:"is_alive,omitempty"`
}

// FromRows scans the sql response data into SocialNetwork.
func (sn *SocialNetwork) FromRows(rows *sql.Rows) error {
	var vsn struct {
		ID      int
		Name    sql.NullString
		APIURL  sql.NullString
		URL     sql.NullString
		IsAlive sql.NullBool
	}
	// the order here should be the same as in the `socialnetwork.Columns`.
	if err := rows.Scan(
		&vsn.ID,
		&vsn.Name,
		&vsn.APIURL,
		&vsn.URL,
		&vsn.IsAlive,
	); err != nil {
		return err
	}
	sn.ID = vsn.ID
	sn.Name = vsn.Name.String
	sn.APIURL = vsn.APIURL.String
	sn.URL = vsn.URL.String
	sn.IsAlive = vsn.IsAlive.Bool
	return nil
}

// QueryProfiles queries the profiles edge of the SocialNetwork.
func (sn *SocialNetwork) QueryProfiles() *ProfileQuery {
	return (&SocialNetworkClient{sn.config}).QueryProfiles(sn)
}

// QueryAccounts queries the accounts edge of the SocialNetwork.
func (sn *SocialNetwork) QueryAccounts() *AccountQuery {
	return (&SocialNetworkClient{sn.config}).QueryAccounts(sn)
}

// Update returns a builder for updating this SocialNetwork.
// Note that, you need to call SocialNetwork.Unwrap() before calling this method, if this SocialNetwork
// was returned from a transaction, and the transaction was committed or rolled back.
func (sn *SocialNetwork) Update() *SocialNetworkUpdateOne {
	return (&SocialNetworkClient{sn.config}).UpdateOne(sn)
}

// Unwrap unwraps the entity that was returned from a transaction after it was closed,
// so that all next queries will be executed through the driver which created the transaction.
func (sn *SocialNetwork) Unwrap() *SocialNetwork {
	tx, ok := sn.config.driver.(*txDriver)
	if !ok {
		panic("ent: SocialNetwork is not a transactional entity")
	}
	sn.config.driver = tx.drv
	return sn
}

// String implements the fmt.Stringer.
func (sn *SocialNetwork) String() string {
	var builder strings.Builder
	builder.WriteString("SocialNetwork(")
	builder.WriteString(fmt.Sprintf("id=%v", sn.ID))
	builder.WriteString(", name=")
	builder.WriteString(sn.Name)
	builder.WriteString(", api_url=")
	builder.WriteString(sn.APIURL)
	builder.WriteString(", url=")
	builder.WriteString(sn.URL)
	builder.WriteString(", is_alive=")
	builder.WriteString(fmt.Sprintf("%v", sn.IsAlive))
	builder.WriteByte(')')
	return builder.String()
}

// SocialNetworks is a parsable slice of SocialNetwork.
type SocialNetworks []*SocialNetwork

// FromRows scans the sql response data into SocialNetworks.
func (sn *SocialNetworks) FromRows(rows *sql.Rows) error {
	for rows.Next() {
		vsn := &SocialNetwork{}
		if err := vsn.FromRows(rows); err != nil {
			return err
		}
		*sn = append(*sn, vsn)
	}
	return nil
}

func (sn SocialNetworks) config(cfg config) {
	for _i := range sn {
		sn[_i].config = cfg
	}
}
