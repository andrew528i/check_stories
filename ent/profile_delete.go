// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"story_check/ent/predicate"
	"story_check/ent/profile"

	"github.com/facebookincubator/ent/dialect/sql"
)

// ProfileDelete is the builder for deleting a Profile entity.
type ProfileDelete struct {
	config
	predicates []predicate.Profile
}

// Where adds a new predicate to the delete builder.
func (pd *ProfileDelete) Where(ps ...predicate.Profile) *ProfileDelete {
	pd.predicates = append(pd.predicates, ps...)
	return pd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (pd *ProfileDelete) Exec(ctx context.Context) (int, error) {
	return pd.sqlExec(ctx)
}

// ExecX is like Exec, but panics if an error occurs.
func (pd *ProfileDelete) ExecX(ctx context.Context) int {
	n, err := pd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (pd *ProfileDelete) sqlExec(ctx context.Context) (int, error) {
	var (
		res     sql.Result
		builder = sql.Dialect(pd.driver.Dialect())
	)
	selector := builder.Select().From(sql.Table(profile.Table))
	for _, p := range pd.predicates {
		p(selector)
	}
	query, args := builder.Delete(profile.Table).FromSelect(selector).Query()
	if err := pd.driver.Exec(ctx, query, args, &res); err != nil {
		return 0, err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(affected), nil
}

// ProfileDeleteOne is the builder for deleting a single Profile entity.
type ProfileDeleteOne struct {
	pd *ProfileDelete
}

// Exec executes the deletion query.
func (pdo *ProfileDeleteOne) Exec(ctx context.Context) error {
	n, err := pdo.pd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &ErrNotFound{profile.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (pdo *ProfileDeleteOne) ExecX(ctx context.Context) {
	pdo.pd.ExecX(ctx)
}
