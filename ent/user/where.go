// Code generated by entc, DO NOT EDIT.

package user

import (
	"story_check/ent/predicate"

	"github.com/facebookincubator/ent/dialect/sql"
)

// ID filters vertices based on their identifier.
func ID(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldID), id))
		},
	)
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldID), id))
		},
	)
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NEQ(s.C(FieldID), id))
		},
	)
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(ids) == 0 {
				s.Where(sql.False())
				return
			}
			v := make([]interface{}, len(ids))
			for i := range v {
				v[i] = ids[i]
			}
			s.Where(sql.In(s.C(FieldID), v...))
		},
	)
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(ids) == 0 {
				s.Where(sql.False())
				return
			}
			v := make([]interface{}, len(ids))
			for i := range v {
				v[i] = ids[i]
			}
			s.Where(sql.NotIn(s.C(FieldID), v...))
		},
	)
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GT(s.C(FieldID), id))
		},
	)
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GTE(s.C(FieldID), id))
		},
	)
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LT(s.C(FieldID), id))
		},
	)
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LTE(s.C(FieldID), id))
		},
	)
}

// MobileGUID applies equality check predicate on the "mobile_guid" field. It's identical to MobileGUIDEQ.
func MobileGUID(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldMobileGUID), v))
		},
	)
}

// TelegramChatID applies equality check predicate on the "telegram_chat_id" field. It's identical to TelegramChatIDEQ.
func TelegramChatID(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramUsername applies equality check predicate on the "telegram_username" field. It's identical to TelegramUsernameEQ.
func TelegramUsername(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldTelegramUsername), v))
		},
	)
}

// Token applies equality check predicate on the "token" field. It's identical to TokenEQ.
func Token(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldToken), v))
		},
	)
}

// MobileGUIDEQ applies the EQ predicate on the "mobile_guid" field.
func MobileGUIDEQ(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDNEQ applies the NEQ predicate on the "mobile_guid" field.
func MobileGUIDNEQ(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NEQ(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDIn applies the In predicate on the "mobile_guid" field.
func MobileGUIDIn(vs ...string) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.In(s.C(FieldMobileGUID), v...))
		},
	)
}

// MobileGUIDNotIn applies the NotIn predicate on the "mobile_guid" field.
func MobileGUIDNotIn(vs ...string) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.NotIn(s.C(FieldMobileGUID), v...))
		},
	)
}

// MobileGUIDGT applies the GT predicate on the "mobile_guid" field.
func MobileGUIDGT(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GT(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDGTE applies the GTE predicate on the "mobile_guid" field.
func MobileGUIDGTE(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GTE(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDLT applies the LT predicate on the "mobile_guid" field.
func MobileGUIDLT(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LT(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDLTE applies the LTE predicate on the "mobile_guid" field.
func MobileGUIDLTE(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LTE(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDContains applies the Contains predicate on the "mobile_guid" field.
func MobileGUIDContains(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.Contains(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDHasPrefix applies the HasPrefix predicate on the "mobile_guid" field.
func MobileGUIDHasPrefix(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.HasPrefix(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDHasSuffix applies the HasSuffix predicate on the "mobile_guid" field.
func MobileGUIDHasSuffix(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.HasSuffix(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDIsNil applies the IsNil predicate on the "mobile_guid" field.
func MobileGUIDIsNil() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.IsNull(s.C(FieldMobileGUID)))
		},
	)
}

// MobileGUIDNotNil applies the NotNil predicate on the "mobile_guid" field.
func MobileGUIDNotNil() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NotNull(s.C(FieldMobileGUID)))
		},
	)
}

// MobileGUIDEqualFold applies the EqualFold predicate on the "mobile_guid" field.
func MobileGUIDEqualFold(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EqualFold(s.C(FieldMobileGUID), v))
		},
	)
}

// MobileGUIDContainsFold applies the ContainsFold predicate on the "mobile_guid" field.
func MobileGUIDContainsFold(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.ContainsFold(s.C(FieldMobileGUID), v))
		},
	)
}

// TelegramChatIDEQ applies the EQ predicate on the "telegram_chat_id" field.
func TelegramChatIDEQ(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramChatIDNEQ applies the NEQ predicate on the "telegram_chat_id" field.
func TelegramChatIDNEQ(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NEQ(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramChatIDIn applies the In predicate on the "telegram_chat_id" field.
func TelegramChatIDIn(vs ...int64) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.In(s.C(FieldTelegramChatID), v...))
		},
	)
}

// TelegramChatIDNotIn applies the NotIn predicate on the "telegram_chat_id" field.
func TelegramChatIDNotIn(vs ...int64) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.NotIn(s.C(FieldTelegramChatID), v...))
		},
	)
}

// TelegramChatIDGT applies the GT predicate on the "telegram_chat_id" field.
func TelegramChatIDGT(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GT(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramChatIDGTE applies the GTE predicate on the "telegram_chat_id" field.
func TelegramChatIDGTE(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GTE(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramChatIDLT applies the LT predicate on the "telegram_chat_id" field.
func TelegramChatIDLT(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LT(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramChatIDLTE applies the LTE predicate on the "telegram_chat_id" field.
func TelegramChatIDLTE(v int64) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LTE(s.C(FieldTelegramChatID), v))
		},
	)
}

// TelegramChatIDIsNil applies the IsNil predicate on the "telegram_chat_id" field.
func TelegramChatIDIsNil() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.IsNull(s.C(FieldTelegramChatID)))
		},
	)
}

// TelegramChatIDNotNil applies the NotNil predicate on the "telegram_chat_id" field.
func TelegramChatIDNotNil() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NotNull(s.C(FieldTelegramChatID)))
		},
	)
}

// TelegramUsernameEQ applies the EQ predicate on the "telegram_username" field.
func TelegramUsernameEQ(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameNEQ applies the NEQ predicate on the "telegram_username" field.
func TelegramUsernameNEQ(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NEQ(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameIn applies the In predicate on the "telegram_username" field.
func TelegramUsernameIn(vs ...string) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.In(s.C(FieldTelegramUsername), v...))
		},
	)
}

// TelegramUsernameNotIn applies the NotIn predicate on the "telegram_username" field.
func TelegramUsernameNotIn(vs ...string) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.NotIn(s.C(FieldTelegramUsername), v...))
		},
	)
}

// TelegramUsernameGT applies the GT predicate on the "telegram_username" field.
func TelegramUsernameGT(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GT(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameGTE applies the GTE predicate on the "telegram_username" field.
func TelegramUsernameGTE(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GTE(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameLT applies the LT predicate on the "telegram_username" field.
func TelegramUsernameLT(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LT(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameLTE applies the LTE predicate on the "telegram_username" field.
func TelegramUsernameLTE(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LTE(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameContains applies the Contains predicate on the "telegram_username" field.
func TelegramUsernameContains(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.Contains(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameHasPrefix applies the HasPrefix predicate on the "telegram_username" field.
func TelegramUsernameHasPrefix(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.HasPrefix(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameHasSuffix applies the HasSuffix predicate on the "telegram_username" field.
func TelegramUsernameHasSuffix(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.HasSuffix(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameIsNil applies the IsNil predicate on the "telegram_username" field.
func TelegramUsernameIsNil() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.IsNull(s.C(FieldTelegramUsername)))
		},
	)
}

// TelegramUsernameNotNil applies the NotNil predicate on the "telegram_username" field.
func TelegramUsernameNotNil() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NotNull(s.C(FieldTelegramUsername)))
		},
	)
}

// TelegramUsernameEqualFold applies the EqualFold predicate on the "telegram_username" field.
func TelegramUsernameEqualFold(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EqualFold(s.C(FieldTelegramUsername), v))
		},
	)
}

// TelegramUsernameContainsFold applies the ContainsFold predicate on the "telegram_username" field.
func TelegramUsernameContainsFold(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.ContainsFold(s.C(FieldTelegramUsername), v))
		},
	)
}

// TokenEQ applies the EQ predicate on the "token" field.
func TokenEQ(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EQ(s.C(FieldToken), v))
		},
	)
}

// TokenNEQ applies the NEQ predicate on the "token" field.
func TokenNEQ(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.NEQ(s.C(FieldToken), v))
		},
	)
}

// TokenIn applies the In predicate on the "token" field.
func TokenIn(vs ...string) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.In(s.C(FieldToken), v...))
		},
	)
}

// TokenNotIn applies the NotIn predicate on the "token" field.
func TokenNotIn(vs ...string) predicate.User {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.User(
		func(s *sql.Selector) {
			// if not arguments were provided, append the FALSE constants,
			// since we can't apply "IN ()". This will make this predicate falsy.
			if len(vs) == 0 {
				s.Where(sql.False())
				return
			}
			s.Where(sql.NotIn(s.C(FieldToken), v...))
		},
	)
}

// TokenGT applies the GT predicate on the "token" field.
func TokenGT(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GT(s.C(FieldToken), v))
		},
	)
}

// TokenGTE applies the GTE predicate on the "token" field.
func TokenGTE(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.GTE(s.C(FieldToken), v))
		},
	)
}

// TokenLT applies the LT predicate on the "token" field.
func TokenLT(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LT(s.C(FieldToken), v))
		},
	)
}

// TokenLTE applies the LTE predicate on the "token" field.
func TokenLTE(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.LTE(s.C(FieldToken), v))
		},
	)
}

// TokenContains applies the Contains predicate on the "token" field.
func TokenContains(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.Contains(s.C(FieldToken), v))
		},
	)
}

// TokenHasPrefix applies the HasPrefix predicate on the "token" field.
func TokenHasPrefix(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.HasPrefix(s.C(FieldToken), v))
		},
	)
}

// TokenHasSuffix applies the HasSuffix predicate on the "token" field.
func TokenHasSuffix(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.HasSuffix(s.C(FieldToken), v))
		},
	)
}

// TokenEqualFold applies the EqualFold predicate on the "token" field.
func TokenEqualFold(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.EqualFold(s.C(FieldToken), v))
		},
	)
}

// TokenContainsFold applies the ContainsFold predicate on the "token" field.
func TokenContainsFold(v string) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			s.Where(sql.ContainsFold(s.C(FieldToken), v))
		},
	)
}

// HasProfiles applies the HasEdge predicate on the "profiles" edge.
func HasProfiles() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			t1 := s.Table()
			builder := sql.Dialect(s.Dialect())
			s.Where(
				sql.In(
					t1.C(FieldID),
					builder.Select(ProfilesPrimaryKey[0]).
						From(builder.Table(ProfilesTable)),
				),
			)
		},
	)
}

// HasProfilesWith applies the HasEdge predicate on the "profiles" edge with a given conditions (other predicates).
func HasProfilesWith(preds ...predicate.Profile) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			builder := sql.Dialect(s.Dialect())
			t1 := s.Table()
			t2 := builder.Table(ProfilesInverseTable)
			t3 := builder.Table(ProfilesTable)
			t4 := builder.Select(t3.C(ProfilesPrimaryKey[0])).
				From(t3).
				Join(t2).
				On(t3.C(ProfilesPrimaryKey[1]), t2.C(FieldID))
			t5 := builder.Select().From(t2)
			for _, p := range preds {
				p(t5)
			}
			t4.FromSelect(t5)
			s.Where(sql.In(t1.C(FieldID), t4))
		},
	)
}

// HasNotifications applies the HasEdge predicate on the "notifications" edge.
func HasNotifications() predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			t1 := s.Table()
			builder := sql.Dialect(s.Dialect())
			s.Where(
				sql.In(
					t1.C(FieldID),
					builder.Select(NotificationsColumn).
						From(builder.Table(NotificationsTable)).
						Where(sql.NotNull(NotificationsColumn)),
				),
			)
		},
	)
}

// HasNotificationsWith applies the HasEdge predicate on the "notifications" edge with a given conditions (other predicates).
func HasNotificationsWith(preds ...predicate.Notification) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			builder := sql.Dialect(s.Dialect())
			t1 := s.Table()
			t2 := builder.Select(NotificationsColumn).From(builder.Table(NotificationsTable))
			for _, p := range preds {
				p(t2)
			}
			s.Where(sql.In(t1.C(FieldID), t2))
		},
	)
}

// And groups list of predicates with the AND operator between them.
func And(predicates ...predicate.User) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			for _, p := range predicates {
				p(s)
			}
		},
	)
}

// Or groups list of predicates with the OR operator between them.
func Or(predicates ...predicate.User) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			for i, p := range predicates {
				if i > 0 {
					s.Or()
				}
				p(s)
			}
		},
	)
}

// Not applies the not operator on the given predicate.
func Not(p predicate.User) predicate.User {
	return predicate.User(
		func(s *sql.Selector) {
			p(s.Not())
		},
	)
}
