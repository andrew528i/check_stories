package schema

import (
	"time"

	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

const (
	ContentTypeStory = "story"
)

// Content holds the schema definition for the Content entity.
type Content struct {
	ent.Schema
}

// Fields of the Content.
func (Content) Fields() []ent.Field {
	return []ent.Field{
		field.Enum("content_type").Values(ContentTypeStory),
		field.String("url"),
		field.String("external_id"),
		field.Enum("media_type").Values("image", "video"),
		field.Time("created_at").Default(time.Now),
	}
}

// Edges of the Content.
func (Content) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("profile", Profile.Type).Ref("contents").Unique(),
		edge.From("account", Account.Type).Ref("checked_content").Unique(),
		edge.To("notifications", Notification.Type),
	}
}
