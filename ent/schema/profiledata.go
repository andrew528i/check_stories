package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// ProfileData holds the schema definition for the ProfileData entity.
type ProfileData struct {
	ent.Schema
}

// Fields of the ProfileData.
func (ProfileData) Fields() []ent.Field {
	return []ent.Field{
		field.Int("value_int").Optional(),
		field.String("value_string").Optional(),
		field.Float("value_float").Optional(),
		field.Bool("value_bool").Optional(),
	}
}

// Edges of the ProfileData.
func (ProfileData) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("profile", Profile.Type).Ref("data").Unique(),
		edge.From("field", ProfileField.Type).Ref("data").Unique(),
	}
}
