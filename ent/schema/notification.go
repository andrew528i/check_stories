package schema

import (
	"time"

	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// Notification holds the schema definition for the Notification entity.
type Notification struct {
	ent.Schema
}

// Fields of the Notification.
func (Notification) Fields() []ent.Field {
	return []ent.Field{
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").UpdateDefault(time.Now),
		field.Enum("status").Values("queued", "processing", "success", "failure"),
		field.Enum("notification_type").Values("telegram", "mobile_app", "site"),
	}
}

// Edges of the Notification.
func (Notification) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).Ref("notifications").Unique(),
		edge.From("content", Content.Type).Ref("notifications"),
		edge.From("profile", Profile.Type).Ref("notifications").Unique(),
	}
}
