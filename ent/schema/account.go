package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// Account holds the schema definition for the Account entity.
type Account struct {
	ent.Schema
}

// Fields of the Account.
func (Account) Fields() []ent.Field {
	return []ent.Field{
		field.String("username"),
		field.String("password"),
		field.Bytes("cookies").Optional(),
		field.Time("last_activity").Optional(),
		field.Enum("status").Values("unknown", "success", "failure"),
	}
}

// Edges of the Account.
func (Account) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("social_network", SocialNetwork.Type).Ref("accounts").Unique(),
		edge.To("checked_content", Content.Type),
	}
}
