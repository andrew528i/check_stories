package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("mobile_guid").Optional().Unique(),
		field.Int64("telegram_chat_id").Optional(),
		field.String("telegram_username").Optional(),
		field.String("token").Unique(),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("profiles", Profile.Type),
		edge.To("notifications", Notification.Type),
	}
}
