package schema

import (
	"time"

	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// Profile holds the schema definition for the Profile entity.
type Profile struct {
	ent.Schema
}

// Fields of the Profile.
func (Profile) Fields() []ent.Field {
	return []ent.Field{
		field.String("username").Unique(),
		field.Time("last_check_at").Default(time.Now),
		field.Enum("status").Values("unknown", "checking", "not_exist", "private", "public"),
	}
}

// Edges of the Profile.
func (Profile) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("users", User.Type).Ref("profiles"),
		edge.From("social_network", SocialNetwork.Type).Ref("profiles").Unique(),
		edge.To("contents", Content.Type),
		edge.To("data", ProfileData.Type),
		edge.To("notifications", Notification.Type),
	}
}
