package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// SocialNetwork holds the schema definition for the SocialNetwork entity.
type SocialNetwork struct {
	ent.Schema
}

// Fields of the SocialNetwork.
func (SocialNetwork) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").Unique(),
		field.String("api_url"),
		field.String("url"),
		field.Bool("is_alive").Default(true),
	}
}

// Edges of the SocialNetwork.
func (SocialNetwork) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("profiles", Profile.Type),
		edge.To("accounts", Account.Type),
	}
}
