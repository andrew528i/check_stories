package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

const (
	ProfileFieldTypeInt = iota
	ProfileFieldTypeString
	ProfileFieldTypeFloat
	ProfileFieldTypeBool
)

// ProfileField holds the schema definition for the ProfileField entity.
type ProfileField struct {
	ent.Schema
}

// Fields of the ProfileField.
func (ProfileField) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").Unique(),
		field.Int8("type"),
	}
}

// Edges of the ProfileField.
func (ProfileField) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("data", ProfileData.Type),
	}
}
