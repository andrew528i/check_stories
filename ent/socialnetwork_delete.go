// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"story_check/ent/predicate"
	"story_check/ent/socialnetwork"

	"github.com/facebookincubator/ent/dialect/sql"
)

// SocialNetworkDelete is the builder for deleting a SocialNetwork entity.
type SocialNetworkDelete struct {
	config
	predicates []predicate.SocialNetwork
}

// Where adds a new predicate to the delete builder.
func (snd *SocialNetworkDelete) Where(ps ...predicate.SocialNetwork) *SocialNetworkDelete {
	snd.predicates = append(snd.predicates, ps...)
	return snd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (snd *SocialNetworkDelete) Exec(ctx context.Context) (int, error) {
	return snd.sqlExec(ctx)
}

// ExecX is like Exec, but panics if an error occurs.
func (snd *SocialNetworkDelete) ExecX(ctx context.Context) int {
	n, err := snd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (snd *SocialNetworkDelete) sqlExec(ctx context.Context) (int, error) {
	var (
		res     sql.Result
		builder = sql.Dialect(snd.driver.Dialect())
	)
	selector := builder.Select().From(sql.Table(socialnetwork.Table))
	for _, p := range snd.predicates {
		p(selector)
	}
	query, args := builder.Delete(socialnetwork.Table).FromSelect(selector).Query()
	if err := snd.driver.Exec(ctx, query, args, &res); err != nil {
		return 0, err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(affected), nil
}

// SocialNetworkDeleteOne is the builder for deleting a single SocialNetwork entity.
type SocialNetworkDeleteOne struct {
	snd *SocialNetworkDelete
}

// Exec executes the deletion query.
func (sndo *SocialNetworkDeleteOne) Exec(ctx context.Context) error {
	n, err := sndo.snd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &ErrNotFound{socialnetwork.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (sndo *SocialNetworkDeleteOne) ExecX(ctx context.Context) {
	sndo.snd.ExecX(ctx)
}
