module story_check

go 1.13

require (
	github.com/facebookincubator/ent v0.0.0-20191102160508-02bae0d6520f
	github.com/go-openapi/inflect v0.19.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1-0.20190510102335-877a9775f068
	github.com/gocelery/gocelery v0.0.0-20191008025616-26dc580ba02e
	github.com/golang/protobuf v1.3.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/mattn/go-runewidth v0.0.5 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271 // indirect
	github.com/tebeka/selenium v0.9.9 // indirect
	go.uber.org/dig v1.7.0
	golang.org/x/net v0.0.0-20191021144547-ec77196f6094 // indirect
	golang.org/x/sys v0.0.0-20191020212454-3e7259c5e7c2 // indirect
	golang.org/x/tools v0.0.0-20191101200257-8dbcdeb83d3f // indirect
	google.golang.org/genproto v0.0.0-20191009194640-548a555dbc03 // indirect
	google.golang.org/grpc v1.24.0
	gopkg.in/tucnak/telebot.v2 v2.0.0-20191005061224-d0707a9d73c4
)
