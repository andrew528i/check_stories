package telegram_handler

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"sync"

	"google.golang.org/grpc/status"
	tb "gopkg.in/tucnak/telebot.v2"

	backend "story_check/service/backend/grpc_handler/pb"
	"story_check/service/common"
)

type ActionType int

const (
	ActionTypeAddProfile ActionType = iota
)

type TelegramHandler struct {
	client    backend.APIServiceClient
	bot       *tb.Bot
	initOnce  sync.Once
	tokens    map[string]string
	actionMap map[string]ActionType // TODO: persist me
}

func NewTelegramHandler(client backend.APIServiceClient, bot *tb.Bot) *TelegramHandler {
	var initOnce sync.Once
	tokens := make(map[string]string)
	actionMap := make(map[string]ActionType)

	return &TelegramHandler{client: client, bot: bot, initOnce: initOnce, tokens: tokens, actionMap: actionMap}
}

func (h TelegramHandler) getToken(chatId int64, username string) (string, error) {
	key := fmt.Sprintf("%d:%s", chatId, username)

	if _, ok := h.tokens[key]; !ok {
		req := &backend.UserSignInRequest{
			Type:             backend.SignInType_TELEGRAM,
			TelegramChatId:   chatId,
			TelegramUsername: username,
		}
		resp, err := h.client.UserSignIn(context.Background(), req)
		if err != nil {
			return "", err
		}

		h.tokens[key] = resp.Token
	}

	return h.tokens[key], nil
}

func (h TelegramHandler) init() {
	profileAddBtn := tb.ReplyButton{Text: "✅ Добавить аккаунт"}
	profileListBtn := tb.ReplyButton{Text: "📒 Список аккаунтов"}
	profileStatsBtn := tb.ReplyButton{Text: "📊 Статистика"}
	mainMenu := [][]tb.ReplyButton{{profileAddBtn}, {profileListBtn}, {profileStatsBtn}}

	h.bot.Handle(&profileAddBtn, func(m *tb.Message) {
		h.bot.Send(m.Sender, "Укажите логин аккаунта", &tb.ReplyMarkup{ReplyKeyboardRemove: true})
		h.actionMap[m.Sender.Username] = ActionTypeAddProfile
	})

	h.bot.Handle(&profileListBtn, func(m *tb.Message) {
		userToken, err := h.getToken(m.Chat.ID, m.Sender.Username)
		if err != nil {
			h.bot.Send(m.Sender, err.Error())
			return
		}

		if err = h.profileList(m.Sender, userToken); err != nil {
			h.bot.Send(m.Sender, err.Error())
		}
	})

	h.bot.Handle(tb.OnCallback, func(c *tb.Callback) {
		h.bot.Respond(c, &tb.CallbackResponse{ShowAlert: false})
		h.processCallback(c)
	})

	h.bot.Handle(tb.OnText, func(m *tb.Message) {
		userToken, err := h.getToken(m.Chat.ID, m.Sender.Username)
		if err != nil {
			h.bot.Send(m.Sender, err.Error())
			return
		}

		username := m.Sender.Username
		answer := ""

		if _, ok := h.actionMap[username]; ok {
			switch h.actionMap[username] {
			case ActionTypeAddProfile:
				profileUsername := m.Text

				if err := h.profileAdd(profileUsername, userToken); err != nil {
					s, _ := status.FromError(err)

					answer = fmt.Sprintf("Не получилось добавить аккаунт: %s", s.Message())
				} else {
					answer = fmt.Sprintf("Аккаунт %s добавлен в очередь на проверку", profileUsername)
				}

			default:
				answer = "Unknown action"
			}

			h.bot.Send(m.Sender, answer, &tb.ReplyMarkup{ReplyKeyboard: mainMenu})

			delete(h.actionMap, username)
			return
		}

		welcomeMessage := "Hello insta stories bla bla bla"
		h.bot.Send(
			m.Sender,
			welcomeMessage,
			&tb.ReplyMarkup{ReplyKeyboard: mainMenu},
		)
	})
}

func (h TelegramHandler) processCallback(c *tb.Callback) {
	data := strings.Split(c.Data, "|")[1]
	splitted := strings.Split(data, " ")
	command := splitted[0]

	userToken, err := h.getToken(c.Message.Chat.ID, c.Message.Chat.Username)
	if err != nil {
		h.bot.Send(c.Sender, err.Error())
		return
	}

	switch command {
	case "remove":
		profileId, err := strconv.Atoi(splitted[1])
		if err != nil {
			h.bot.Send(c.Sender, err.Error())
			return
		}

		if err = h.profileRemove(profileId, userToken); err != nil {
			h.bot.Send(c.Sender, fmt.Sprintf("❌ Не получилось удалить аккаунт: %v", err))
		} else {
			h.bot.Send(c.Sender, "✅ Успешно отписались от аккаунта")
		}
	}
}

func (h TelegramHandler) profileAdd(username, userToken string) error {
	req := &backend.ProfileAddRequest{Username: strings.ToLower(username)}
	common.SetUserToken(req, userToken)

	_, err := h.client.ProfileAdd(context.Background(), req)

	return err
}

func (h TelegramHandler) profileRemove(profileId int, userToken string) error {
	req := &backend.ProfileRemoveRequest{ProfileId: int64(profileId)}
	common.SetUserToken(req, userToken)

	_, err := h.client.ProfileRemove(context.Background(), req)

	return err
}

func (h TelegramHandler) profileList(sender *tb.User, userToken string) error {
	req := &backend.ProfileListRequest{}
	common.SetUserToken(req, userToken)

	resp, err := h.client.ProfileList(context.Background(), req)
	if err != nil {
		return err
	}

	var profiles []string

	for _, p := range resp.Profiles {
		profiles = append(profiles, p.Username)
	}

	h.bot.Send(sender, strings.Join(profiles, "\n"))

	return err
}

func (h TelegramHandler) Start() {
	h.initOnce.Do(func() {
		h.init()
	})

	h.bot.Start()
}
