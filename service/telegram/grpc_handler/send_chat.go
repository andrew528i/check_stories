package grpc_handler

import (
	"context"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	uuid "github.com/satori/go.uuid"
	tb "gopkg.in/tucnak/telebot.v2"

	telegram "story_check/service/telegram/grpc_handler/pb"
)

const TelegramAttachmentLimit = 10 // TODO: to the config

func (h TelegramAPIHandler) SendChat(_ context.Context, req *telegram.SendChatRequest) (*empty.Empty, error) {
	msg := req.Message
	user := &tb.User{ID: int(req.ChatId), Username: req.Username}
	attachmentsLen := len(msg.Attachments)

	if msg.Attachments == nil {
		_, err := h.bot.Send(user, msg.Body)

		return &empty.Empty{}, err
	}

	for i, a := range msg.Attachments {
		var contentMenu [][]tb.InlineButton
		for title, data := range a.Data {
			u, _ := uuid.NewV4()
			contentMenu = append(contentMenu, []tb.InlineButton{{
				Unique: u.String(),
				Text:   title,
				Data:   data,
			}})
		}

		sendOptions := []interface{}{&tb.ReplyMarkup{InlineKeyboard: contentMenu}}
		if attachmentsLen > 1 && i != (attachmentsLen-1) {
			sendOptions = append(sendOptions, tb.Silent) // notify only on last story
		}

		var media tb.InputMedia

		switch a.Type {
		case telegram.MediaType_VIDEO:
			media = &tb.Video{File: tb.FromURL(a.Url), Caption: a.Caption}

		case telegram.MediaType_IMAGE:
			media = &tb.Photo{File: tb.FromURL(a.Url), Caption: a.Caption}

		default:
			return nil, fmt.Errorf("unknown album attachment type: %d", a.Type)
		}

		_, err := h.bot.Send(user, media, sendOptions...)

		if err != nil {
			return &empty.Empty{}, err
		}
	}

	//for i := 0; i < attachmentsLen; i += TelegramAttachmentLimit {
	//	j := i + TelegramAttachmentLimit
	//	if j > attachmentsLen {
	//		j = attachmentsLen
	//	}
	//
	//	var album []tb.InputMedia
	//
	//	for _, a := range msg.Attachments[i:j] {
	//		var media tb.InputMedia
	//
	//		switch a.Type {
	//		case telegram.MediaType_VIDEO:
	//			media = &tb.Video{File: tb.FromURL(a.Url), Caption: a.Caption}
	//
	//		case telegram.MediaType_IMAGE:
	//			media = &tb.Photo{File: tb.FromURL(a.Url), Caption: a.Caption}
	//
	//		default:
	//			return nil, fmt.Errorf("unknown album attachment type: %d", a.Type)
	//		}
	//
	//		album = append(album, media)
	//	}
	//
	//
	//
	//	addProfileBtn := tb.InlineButton{Text: "✅ Добавить аккаунт", Unique: "profile_add"}
	//	listProfileBtn := tb.InlineButton{Text: "📒 Список аккаунтов", Unique: "profile_list"}
	//	mainMenu := [][]tb.InlineButton{{addProfileBtn, listProfileBtn}}
	//
	//	_, err := h.bot.SendAlbum(user, album, &tb.ReplyMarkup{
	//		InlineKeyboard: mainMenu,
	//	}) // TODO: don't forget about msg.Body
	//	if err != nil {
	//		return &empty.Empty{}, err
	//	}
	//
	//	//if len(album) > 1 {
	//	//	_, err := h.bot.Send(user, msg.Body)
	//	//	if err != nil {
	//	//		return &empty.Empty{}, err
	//	//	}
	//	//}
	//}

	return &empty.Empty{}, nil
}
