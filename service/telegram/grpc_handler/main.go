package grpc_handler

import (
	tb "gopkg.in/tucnak/telebot.v2"
)

type TelegramAPIHandler struct {
	bot *tb.Bot
}

func NewTelegramAPIHandler(bot *tb.Bot) *TelegramAPIHandler {
	return &TelegramAPIHandler{bot: bot}
}
