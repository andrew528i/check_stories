package telegram

import (
	"net/http"
	"net/url"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"

	"story_check/config"
)

func getClient() (*http.Client, error) {
	proxyStr := config.TELEGRAM_PROXY
	if proxyStr == "" {
		return http.DefaultClient, nil
	}

	proxyURL, err := url.Parse(proxyStr)
	if err != nil {
		return nil, err
	}

	transport := &http.Transport{Proxy: http.ProxyURL(proxyURL)}

	return &http.Client{Transport: transport}, nil
}

func NewBot() *tb.Bot {
	client, err := getClient()
	if err != nil {
		panic(err)
	}

	bot, err := tb.NewBot(tb.Settings{
		Token:  config.TELEGRAM_BOT_TOKEN,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
		Client: client,
	})
	if err != nil {
		panic(err)
	}

	return bot
}
