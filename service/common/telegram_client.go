package common

import (
	"fmt"

	"google.golang.org/grpc"

	"story_check/config"
	telegram "story_check/service/telegram/grpc_handler/pb"
)

func NewTelegramClient() telegram.TelegramAPIServiceClient {
	target := fmt.Sprintf("%s:%d", config.TELEGRAM_HOST, config.TELEGRAM_PORT)
	conn, err := grpc.Dial(target, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	return telegram.NewTelegramAPIServiceClient(conn)
}
