package dao

import (
	"context"
	"time"

	"story_check/ent"
	"story_check/ent/account"
)

type Account struct {
	db *ent.Client
}

func NewAccountDao(db *ent.Client) *Account {
	return &Account{db: db}
}

func (d Account) GetLastUsed(status account.Status) (*ent.Account, error) {
	ctx := context.Background()

	a, err := d.db.Account.Query().
		Where(
			account.StatusEQ(status),
		).
		Order(
			ent.Desc(account.FieldLastActivity),
		).
		First(ctx)
	if err != nil {
		return nil, err
	}

	return a.Update().
		SetLastActivity(time.Now()).
		Save(ctx)
}
