package dao

import (
	"context"

	"story_check/ent"
	"story_check/ent/content"
	"story_check/ent/notification"
)

type Notification struct {
	db *ent.Client
}

func NewNotificationDao(db *ent.Client) *Notification {
	return &Notification{db: db}
}

func (d Notification) GetUnnotifiedContent() ([]*ent.Content, error) {
	ctx := context.Background()

	return d.db.Content.Query().Where(
		content.Not(content.HasNotifications()),
	).All(ctx)
}

func (d Notification) GetByStatus(status notification.Status) ([]*ent.Notification, error) {
	ctx := context.Background()

	return d.db.Notification.Query().
		Where(
			notification.StatusEQ(status),
		).
		All(ctx)
}

func (d Notification) SetStatus(n []*ent.Notification, status notification.Status) error {
	ctx := context.Background()
	var notificationIds []int

	for _, n_ := range n {
		notificationIds = append(notificationIds, n_.ID)
	}

	_, err := d.db.Notification.Update().
		Where(
			notification.IDIn(notificationIds...),
		).
		SetStatus(status).
		Save(ctx)

	return err
}
