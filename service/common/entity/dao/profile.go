package dao

import (
	"context"
	"errors"
	"time"

	"story_check/ent"
	"story_check/ent/content"
	"story_check/ent/profile"
)

type Profile struct {
	db *ent.Client
}

func NewProfileDao(db *ent.Client) *Profile {
	return &Profile{db: db}
}

func (d Profile) Create(username string, u *ent.User) (p *ent.Profile, err error) {
	ctx := context.Background()
	profileQuery := d.db.Profile.Query().
		Where(profile.Username(username)).
		Clone()
	exist, err := profileQuery.Exist(ctx)
	if err != nil {
		return nil, err
	}

	if !exist {
		p, err = d.db.Profile.Create().
			SetUsername(username).
			SetStatus(profile.StatusUnknown).
			SetLastCheckAt(time.Now()).
			Save(ctx)
	} else {
		p, err = profileQuery.Only(ctx)
	}

	if err != nil {
		return nil, err
	}

	users, err := p.QueryUsers().All(ctx)
	if err != nil {
		return nil, err
	}

	for _, profileUser := range users {
		if profileUser.ID == u.ID {
			return nil, errors.New("already_exist")
		}
	}

	return p.Update().AddUsers(u).Save(ctx)
}

func (d Profile) GetByStatus(status profile.Status, timeout int) ([]*ent.Profile, error) {
	ctx := context.Background()
	t := time.Now().Add(time.Second * time.Duration(-1*timeout))

	return d.db.Profile.Query().
		Where(
			profile.StatusEQ(status),
			profile.LastCheckAtLTE(t),
		).
		All(ctx)
}

func (d Profile) GetByContent(c *ent.Content) (*ent.Profile, error) {
	ctx := context.Background()

	return c.QueryProfile().Only(ctx)
}

func (d Profile) GetUsers(p *ent.Profile) ([]*ent.User, error) {
	ctx := context.Background()

	return p.QueryUsers().All(ctx)
}

func (d Profile) SetStatus(p *ent.Profile, status profile.Status) error {
	ctx := context.Background()

	_, err := p.Update().
		SetStatus(status).
		SetLastCheckAt(time.Now()).
		Save(ctx)

	return err
}

func (d Profile) SetStatusOutdated(status, newStatus profile.Status, timeout int) error {
	ctx := context.Background()
	t := time.Now().Add(time.Second * time.Duration(-1*timeout))

	_, err := d.db.Profile.Update().Where(
		profile.StatusEQ(status),
		profile.LastCheckAtLTE(t),
	).SetStatus(newStatus).
		SetLastCheckAt(time.Now()).
		Save(ctx)

	return err
}

func (d Profile) GetLastContent(t content.ContentType, p *ent.Profile, timeout int) ([]*ent.Content, error) {
	ctx := context.Background()
	dt := time.Now().Add(time.Second * time.Duration(-1*timeout))

	return d.db.Content.Query().
		Where(
			content.ContentTypeEQ(t),
			content.HasProfileWith(
				profile.ID(p.ID),
			),
			content.CreatedAtGTE(dt),
		).Order(
		ent.Desc(content.FieldCreatedAt),
	).All(ctx)
}

func (d Profile) CheckExistingContent(p *ent.Profile, externalId ...string) ([]string, error) {
	ctx := context.Background()
	var result []string

	contents, err := d.db.Content.Query().Where(
		content.ExternalIDIn(externalId...),
		content.HasProfileWith(
			profile.ID(p.ID),
		),
	).All(ctx)
	if err != nil {
		return nil, err
	}

	for _, c := range contents {
		result = append(result, c.ExternalID)
	}

	return result, nil
}

func (d Profile) RefreshCheckedAt(p *ent.Profile) error {
	ctx := context.Background()
	_, err := p.Update().SetLastCheckAt(time.Now()).Save(ctx)

	return err
}
