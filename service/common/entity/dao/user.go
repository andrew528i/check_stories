package dao

import (
	"context"
	"sync"

	uuid "github.com/satori/go.uuid"

	"story_check/ent"
	"story_check/ent/user"
	"story_check/service/backend/grpc_handler/util"
)

type User struct {
	db                *ent.Client
	tokenUserCache    map[string]*ent.User
	tokenUserCacheMut sync.Mutex
}

func NewUserDao(db *ent.Client) *User {
	tokenUserCache := make(map[string]*ent.User)
	tokenUserCacheMut := sync.Mutex{}

	return &User{db: db, tokenUserCache: tokenUserCache, tokenUserCacheMut: tokenUserCacheMut}
}

func (d User) GetOrCreateTelegram(chatId int64, username string) (*ent.User, error) {
	userQuery := d.db.User.Query().
		Where(
			user.TelegramChatID(chatId),
			user.TelegramUsername(username),
		).
		Clone()

	exist, err := userQuery.Exist(context.Background())
	if err != nil {
		return nil, err
	}

	if exist {
		return userQuery.Only(context.Background())
	}

	token, _ := uuid.NewV4()

	return d.db.User.Create().
		SetTelegramChatID(chatId).
		SetTelegramUsername(username).
		SetToken(token.String()).
		Save(context.Background())
}

func (d User) GetByToken(token string) (*ent.User, error) {
	ctx := context.Background()
	if u, ok := d.tokenUserCache[token]; ok {
		return u, nil
	}

	u, err := d.db.User.Query().
		Where(user.Token(token)).
		Only(ctx)
	if err != nil {
		return nil, err
	}

	d.tokenUserCacheMut.Lock()
	d.tokenUserCache[token] = u
	defer d.tokenUserCacheMut.Unlock()

	return u, nil
}

func (d User) GetFromContext(ctx context.Context) (*ent.User, error) {
	token, err := util.GetTokenFromContext(ctx)
	if err != nil {
		return nil, err
	}

	return d.GetByToken(token)
}
