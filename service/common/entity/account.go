package entity

import (
	"story_check/ent"
	"story_check/ent/account"
)

type AccountDao interface {
	GetLastUsed(status account.Status) (*ent.Account, error)
}
