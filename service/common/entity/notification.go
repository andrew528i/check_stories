package entity

import (
	"story_check/ent"
	"story_check/ent/notification"
)

type NotificationDao interface {
	GetUnnotifiedContent() ([]*ent.Content, error)
	GetByStatus(status notification.Status) ([]*ent.Notification, error)
	SetStatus(n []*ent.Notification, status notification.Status) error
}
