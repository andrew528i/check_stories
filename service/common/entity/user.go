package entity

import (
	"context"

	"story_check/ent"
)

type UserDao interface {
	GetOrCreateTelegram(chatId int64, username string) (*ent.User, error)
	GetByToken(token string) (*ent.User, error)
	GetFromContext(ctx context.Context) (*ent.User, error)
}
