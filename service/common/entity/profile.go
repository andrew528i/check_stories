package entity

import (
	"story_check/ent"
	"story_check/ent/content"
	"story_check/ent/profile"
)

type ProfileDao interface {
	Create(username string, u *ent.User) (*ent.Profile, error)
	GetByStatus(status profile.Status, timeout int) ([]*ent.Profile, error)
	GetByContent(c *ent.Content) (*ent.Profile, error)
	GetUsers(p *ent.Profile) ([]*ent.User, error)
	SetStatus(p *ent.Profile, status profile.Status) error
	SetStatusOutdated(status, newStatus profile.Status, timeout int) error
	GetLastContent(t content.ContentType, p *ent.Profile, timeout int) ([]*ent.Content, error)
	CheckExistingContent(p *ent.Profile, externalId ...string) ([]string, error)
	RefreshCheckedAt(p *ent.Profile) error
}
