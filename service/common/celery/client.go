package celery

import (
	"time"

	"github.com/gocelery/gocelery"
	"github.com/gomodule/redigo/redis"
)

func NewClient(backendUrl string) (*gocelery.CeleryClient, error) {
	redisPool := &redis.Pool{
		MaxIdle:     5,                 // maximum number of idle connections in the pool
		MaxActive:   0,                 // maximum number of connections allocated by the pool at a given time
		IdleTimeout: 240 * time.Second, // close connections after remaining idle for this duration
		Dial: func() (redis.Conn, error) {
			c, err := redis.DialURL(backendUrl)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}

	return gocelery.NewCeleryClient(
		//gocelery.NewAMQPCeleryBroker("amqp://rabbitmq:5672"),
		gocelery.NewRedisBroker(redisPool),
		gocelery.NewRedisBackend(redisPool),
		100,
	)
}
