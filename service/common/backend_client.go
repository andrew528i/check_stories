package common

import (
	"context"
	"fmt"
	"sync"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"story_check/config"
	backend "story_check/service/backend/grpc_handler/pb"
)

var reqTokenMap map[interface{}]string
var reqTokenMapOnce sync.Once
var reqTokenMapMut sync.Mutex

func SetUserToken(req interface{}, token string) {
	reqTokenMapOnce.Do(func() {
		reqTokenMap = make(map[interface{}]string)
		reqTokenMapMut = sync.Mutex{}
	})

	reqTokenMapMut.Lock()
	defer reqTokenMapMut.Unlock()
	reqTokenMap[req] = token
}

func NewBackendClient() backend.APIServiceClient {
	target := fmt.Sprintf("%s:%d", config.BACKEND_HOST, config.BACKEND_PORT)
	conn, err := grpc.Dial(
		target,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(interceptor),
	)
	if err != nil {
		panic(err)
	}

	return backend.NewAPIServiceClient(conn)
}

func interceptor(ctx context.Context, method string, req interface{}, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	if token, ok := reqTokenMap[req]; ok {
		ctx = metadata.AppendToOutgoingContext(ctx, config.GRPC_TOKEN_HEADER, token)
	}

	return invoker(ctx, method, req, reply, cc, opts...)
}
