package di

import (
	"database/sql"

	"story_check/config"
	"story_check/ent"
	"story_check/service/common/entity"
	"story_check/service/common/entity/dao"

	_ "github.com/go-sql-driver/mysql"
)

func InitContainer() {
	// database
	Provide(func() *ent.Client {
		client, err := ent.Open("mysql", config.MYSQL_CONNECTION_URL) //, ent.Debug())
		if err != nil {
			panic(err)
		}

		return client
	})
	Provide(func() *sql.DB {
		db, err := sql.Open("mysql", config.MYSQL_CONNECTION_URL)
		if err != nil {
			panic(err)
		}

		return db
	})

	// dao
	Provide(dao.NewUserDao)
	Provide(func(dao *dao.User) entity.UserDao {
		return dao
	})

	Provide(dao.NewProfileDao)
	Provide(func(dao *dao.Profile) entity.ProfileDao {
		return dao
	})

	Provide(dao.NewAccountDao)
	Provide(func(dao *dao.Account) entity.AccountDao {
		return dao
	})

	Provide(dao.NewNotificationDao)
	Provide(func(dao *dao.Notification) entity.NotificationDao {
		return dao
	})
}
