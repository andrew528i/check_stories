package di

import (
	"sync"

	"go.uber.org/dig"
)

var container *dig.Container
var once sync.Once

func Provide(constructor interface{}, opts ...dig.ProvideOption) {
	container := GetDic()

	err := container.Provide(constructor, opts...)
	if err != nil {
		panic(err) // TODO: log me
	}
}

func GetDic() *dig.Container {
	once.Do(func() {
		container = dig.New()
	})

	return container
}

func Invoke(function interface{}, opts ...dig.InvokeOption) {
	container := GetDic()

	err := container.Invoke(function, opts...)
	if err != nil {
		panic(err) // TODO: log me
	}
}
