package watcher

import (
	"context"
	"fmt"
	"sync"
	"time"

	"story_check/ent"
	"story_check/ent/content"
	"story_check/ent/notification"
	"story_check/ent/profile"
	"story_check/service/common/entity"
	telegram "story_check/service/telegram/grpc_handler/pb"
)

type NotificationWatcher struct {
	db *ent.Client

	telegramClient telegram.TelegramAPIServiceClient

	notificationDao entity.NotificationDao
	profileDao      entity.ProfileDao
}

func NewNotificationWatcher(
	db *ent.Client,
	telegramClient telegram.TelegramAPIServiceClient,
	notificationDao entity.NotificationDao,
	profileDao entity.ProfileDao,
) *NotificationWatcher {
	return &NotificationWatcher{
		db:              db,
		telegramClient:  telegramClient,
		notificationDao: notificationDao,
		profileDao:      profileDao,
	}
}

func (w NotificationWatcher) updateNotifications() error {
	ctx := context.Background()
	contents, err := w.notificationDao.GetUnnotifiedContent()
	if err != nil {
		return err
	}

	tx, err := w.db.Tx(ctx) // transaction for notifications
	if err != nil {
		return err
	}

	notificationMap := make(map[string]*ent.NotificationCreate)
	getNotification := func(tx *ent.Tx, u *ent.User, p *ent.Profile) *ent.NotificationCreate {
		key := fmt.Sprintf("%d_%d", u.ID, p.ID)
		if nc, ok := notificationMap[key]; ok {
			return nc
		}

		notificationMap[key] = tx.Notification.Create().
			SetUser(u).
			SetProfile(p).
			SetNotificationType(notification.NotificationTypeTelegram). // TODO: user settings for notification types
			SetStatus(notification.StatusQueued).
			SetUpdatedAt(time.Now())

		return notificationMap[key]
	}

	for _, c := range contents {
		p, err := w.profileDao.GetByContent(c)
		if err != nil {
			return err
		}

		users, err := w.profileDao.GetUsers(p)
		if err != nil {
			return err
		}

		for _, u := range users {
			getNotification(tx, u, p).AddContent(c)
		}
	}

	for _, v := range notificationMap {
		_, err = v.Save(ctx)
		if err != nil {
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		if rErr := tx.Rollback(); rErr != nil {
			return rErr
		}

		return err
	}

	return nil
}

func (w NotificationWatcher) NotifyUsers() error {
	ctx := context.Background()
	notifications, err := w.notificationDao.GetByStatus(notification.StatusQueued)
	if err != nil {
		return err
	}

	if notifications == nil {
		return nil
	}

	if err = w.notificationDao.SetStatus(notifications, notification.StatusProcessing); err != nil {
		return err
	}

	var wg sync.WaitGroup

	for _, n := range notifications {
		u, err := n.QueryUser().Only(ctx) // TODO: -> dao
		if err != nil {
			return err
		}

		p, err := n.QueryProfile().Only(ctx) // TODO: -> dao
		if err != nil {
			return err
		}

		contents, err := n.QueryContent().All(ctx)
		if err != nil {
			return err
		}

		wg.Add(1)

		go func() {
			defer wg.Done()

			if err = w.NotifyUser(u, p, contents); err != nil {
				fmt.Println("error notifying user, ", err)
				w.notificationDao.SetStatus([]*ent.Notification{n}, notification.StatusFailure)
			} else {
				w.notificationDao.SetStatus([]*ent.Notification{n}, notification.StatusSuccess)
			}
		}()
	}

	wg.Wait()

	return nil
}

func (w NotificationWatcher) NotifyUser(u *ent.User, p *ent.Profile, c []*ent.Content) error {
	ctx := context.Background()
	var attachments []*telegram.Media

	for _, c_ := range c {
		formattedTime := c_.CreatedAt.Format("15:04, 02 Jan 2006")
		caption := fmt.Sprintf("%s added story at %s", p.Username, formattedTime)
		attachment := &telegram.Media{
			Url:     c_.URL,
			Caption: caption,
			Data: map[string]string{
				"❌ отписаться": fmt.Sprintf("remove %d", p.ID),
			},
		}

		switch c_.MediaType {
		case content.MediaTypeImage:
			attachment.Type = telegram.MediaType_IMAGE

		case content.MediaTypeVideo:
			attachment.Type = telegram.MediaType_VIDEO
		}

		attachments = append(attachments, attachment)
	}

	if attachments == nil {
		return fmt.Errorf("attachments is nil, really strange")
	}

	msg := telegram.TelegramMessage{
		Body:        fmt.Sprintf("Пользователь %s добавил сторис: ", p.Username),
		Attachments: attachments,
	}

	req := &telegram.SendChatRequest{
		ChatId:   u.TelegramChatID,
		Username: u.TelegramUsername,
		Message:  &msg,
	}

	if _, err := w.telegramClient.SendChat(ctx, req); err != nil {
		return err
	}

	return nil
}

func (w NotificationWatcher) NotifyProfileStatus(p *ent.Profile, status profile.Status) error {
	ctx := context.Background()
	msg := ""

	switch status {
	case profile.StatusPublic:
		msg = fmt.Sprintf("Аккаунт %s публичный и успешно добавлен 🙂", p.Username)

	case profile.StatusPrivate:
		msg = fmt.Sprintf("К сожалению аккаунт %s закрытый 😟", p.Username)

	case profile.StatusNotExist:
		msg = fmt.Sprintf("Аккаунт %s не найден 😨", p.Username)

	default:
		msg = fmt.Sprintf("Не получилось проверить аккаунт %s", p.Username)
	}

	users, err := p.QueryUsers().All(ctx)
	if err != nil {
		return err
	}

	for _, u := range users {
		req := &telegram.SendChatRequest{
			ChatId:   u.TelegramChatID,
			Username: u.TelegramUsername,
			Message:  &telegram.TelegramMessage{Body: msg},
		}
		_, err := w.telegramClient.SendChat(ctx, req)
		if err != nil {
			return err
		}
	}

	return nil
}

func (w NotificationWatcher) Run() {
	for {
		if err := w.updateNotifications(); err != nil {
			fmt.Println("error updating notifications", err)
		}

		if err := w.NotifyUsers(); err != nil {
			fmt.Println("error notifying users", err)
		}

		time.Sleep(time.Second * 5)
	}
}
