package watcher

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/gocelery/gocelery"

	"story_check/config"
	"story_check/ent"
	"story_check/ent/content"
	"story_check/ent/profile"
	"story_check/service/common/celery"
	"story_check/service/common/entity"
	"story_check/service/watcher/model/instagram"
)

type ProfileWatcher struct {
	db *ent.Client

	profileCeleryClient      *gocelery.CeleryClient
	profileStoryCeleryClient *gocelery.CeleryClient
	profileDao               entity.ProfileDao

	accountWatcher      *AccountWatcher
	notificationWatcher *NotificationWatcher
}

func NewProfileWatcher(
	db *ent.Client,
	profileDao entity.ProfileDao,
	accountWatcher *AccountWatcher,
	notificationWatcher *NotificationWatcher,
) *ProfileWatcher {
	profileCeleryClient, err := celery.NewClient("redis://redis:6379/1") // TODO: -> di cont
	if err != nil {
		panic(err)
	}

	profileStoryCeleryClient, err := celery.NewClient("redis://redis:6379/2") // TODO: -> di cont
	if err != nil {
		panic(err)
	}

	return &ProfileWatcher{
		db:                       db,
		profileCeleryClient:      profileCeleryClient,
		profileStoryCeleryClient: profileStoryCeleryClient,
		profileDao:               profileDao,
		accountWatcher:           accountWatcher,
		notificationWatcher:      notificationWatcher,
	}
}

func (w ProfileWatcher) CheckProfile(a *instagram.Account, p *ent.Profile) error {
	serialized, err := json.Marshal(a)
	if err != nil {
		return err
	}

	asyncResult, err := w.profileCeleryClient.Delay("app.instagram_check_profile", string(serialized), p.Username)
	if err != nil {
		return err
	}

	result, err := asyncResult.Get(time.Second * 60) // TODO: timeout -> config
	if err != nil {
		return err
	}

	if result == nil {
		fmt.Println(result, err, "FUCK")
		return fmt.Errorf("result is nil: %v", asyncResult.TaskID)
	}

	// changing status after profile check
	profileStatus := profile.Status(result.(string))

	if err = w.profileDao.SetStatus(p, profileStatus); err != nil {
		return err
	}

	if err = w.notificationWatcher.NotifyProfileStatus(p, profileStatus); err != nil {
		return fmt.Errorf("error notifying users on profile: %s - %v", p.Username, err)
	}

	return nil
}

func (w ProfileWatcher) CheckProfiles() {
	//refreshing outdated profiles with "checking" status
	err := w.profileDao.SetStatusOutdated(profile.StatusChecking, profile.StatusUnknown, config.WATCHER_CHECKING_TIMEOUT)
	if err != nil {
		fmt.Println("update outdated profile checkng statuses", err)
		return
	}

	// checking profiles with unknown status
	profiles, err := w.profileDao.GetByStatus(profile.StatusUnknown, config.WATCHER_CHECKING_TIMEOUT)
	if err != nil {
		fmt.Println(err)
		return
	}

	var wg sync.WaitGroup

	for _, p := range profiles {
		pCopy := p
		wg.Add(1)

		go func() {
			defer wg.Done()

			a, err := w.accountWatcher.PickAccount()
			if err != nil {
				fmt.Println("pick account", err)
				return
			}

			if err = w.profileDao.SetStatus(pCopy, profile.StatusChecking); err != nil {
				fmt.Println("check profile status", err)
				return
			}

			fmt.Println("Checking ", pCopy.Username, " with ", a)

			acc := instagram.Account{Id: a.ID, Cookies: string(a.Cookies)}
			if err = w.CheckProfile(&acc, pCopy); err != nil {
				fmt.Println("cannot check account: ", err)
			}
		}()
	}

	wg.Wait()
}

func (w ProfileWatcher) CheckProfileStory(a *instagram.Account, p *ent.Profile) ([]*instagram.Story, error) {
	var stories []*instagram.Story

	// serializing account for celery task
	serialized, err := json.Marshal(a)
	if err != nil {
		return nil, err
	}

	// sending task -> celery
	asyncResult, err := w.profileStoryCeleryClient.Delay("app.instagram_check_profile_story", string(serialized), p.Username)
	if err != nil {
		return nil, err
	}

	// waiting for result
	result, err := asyncResult.Get(time.Second * 60) // TODO: timeout -> config
	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, fmt.Errorf("check story result is nil")
	}

	// deserializing result
	return stories, json.Unmarshal([]byte(result.(string)), &stories)
}

func (w ProfileWatcher) CheckProfileStories() {
	storyArgs := make(map[int][]*instagram.StoryArgs) // profile.ID -> []Story
	storiesSyncMut := sync.Mutex{}

	// picking profile to check
	profiles, err := w.profileDao.GetByStatus(profile.StatusPublic, config.WATCHER_STORY_CHECKING_TIMEOUT)
	if err != nil {
		fmt.Println("get profile for story check", err)
		return
	}

	batchSize := config.WATCHER_STORY_CHECK_BATCH_SIZE
	profileSize := len(profiles)

	for i := 0; i < profileSize; i += batchSize {
		j := i + batchSize
		if j > profileSize {
			j = profileSize
		}

		var wg sync.WaitGroup

		for _, p_ := range profiles[i:j] {
			p := p_
			wg.Add(1)

			go func() {
				defer wg.Done()

				a, err := w.accountWatcher.PickAccount()
				if err != nil {
					fmt.Println("pick account in story check error: ", err)
					return
				}

				acc := &instagram.Account{Id: a.ID, Cookies: string(a.Cookies)}
				stories, err := w.CheckProfileStory(acc, p)
				if err != nil {
					fmt.Println("error checking profile", p.Username, " with ", a.Username, err)
					return
				}

				if stories != nil {
					args := &instagram.StoryArgs{Stories: stories, Account: a, Profile: p}

					storiesSyncMut.Lock()
					if _, ok := storyArgs[p.ID]; ok {
						storyArgs[p.ID] = append(storyArgs[p.ID], args)
					} else {
						storyArgs[p.ID] = []*instagram.StoryArgs{args}
					}
					storiesSyncMut.Unlock()
				}
			}()
		}

		wg.Wait()

		if storyArgs != nil {
			if err = w.SyncStories(storyArgs); err != nil {
				fmt.Println("sync stories error", err)
			}
		}
	}
}

// TODO: move me to the dao
func (w ProfileWatcher) SyncStories(args map[int][]*instagram.StoryArgs) error {
	ctx := context.Background()
	tx, err := w.db.Tx(ctx)
	if err != nil {
		return err
	}

	for _, storyArgs := range args {
		var externalIds []string
		sa := storyArgs[0]

		for _, s := range sa.Stories {
			externalIds = append(externalIds, s.Id)
		}

		existing, err := w.profileDao.CheckExistingContent(sa.Profile, externalIds...)
		if err != nil {
			return err
		}
		existingStr := strings.Join(existing, "|")
		//var notifyStories []*instagram.Story

		for _, s := range sa.Stories {
			if strings.Contains(existingStr, s.Id) {
				fmt.Println("story already exists")
				continue
			}

			var mediaType content.MediaType

			if s.IsVideo {
				mediaType = content.MediaTypeVideo
			} else {
				mediaType = content.MediaTypeImage
			}

			fmt.Println("adding new story")
			createdAt := time.Unix(s.TakenAt, 0)
			_, err = tx.Content.Create().
				SetContentType(content.ContentTypeStory).
				SetAccount(sa.Account).
				SetCreatedAt(createdAt).
				SetExternalID(s.Id).
				SetURL(s.Src).
				SetProfile(sa.Profile).
				SetMediaType(mediaType).
				Save(ctx)
			if err != nil {
				return err
			}
		}

		defer func() {
			if err = w.profileDao.RefreshCheckedAt(sa.Profile); err != nil {
				fmt.Println("error refreshing last_checked_at: ", err)
			}
		}()
	}

	if err = tx.Commit(); err != nil {
		if rErr := tx.Rollback(); rErr != nil {
			return rErr
		}

		return err
	}

	return nil
}

func (w ProfileWatcher) RunCheckProfiles() {
	for {
		w.CheckProfiles()

		time.Sleep(time.Second * 10)
	}
}

func (w ProfileWatcher) RunCheckProfileStories() {
	for {
		w.CheckProfileStories()

		//time.Sleep(time.Minute * 5)
		time.Sleep(time.Second * 5)
	}
}
