package watcher

import (
	"sync"

	"story_check/ent"
	"story_check/ent/account"
	"story_check/service/common/entity"
)

type AccountWatcher struct {
	accountDao entity.AccountDao

	pickAccountMut *sync.Mutex
}

func NewAccountWatcher(accountDao entity.AccountDao) *AccountWatcher {
	pickAccountMut := &sync.Mutex{}

	return &AccountWatcher{accountDao: accountDao, pickAccountMut: pickAccountMut}
}

func (w AccountWatcher) PickAccount() (*ent.Account, error) {
	w.pickAccountMut.Lock()
	defer w.pickAccountMut.Unlock()

	return w.accountDao.GetLastUsed(account.StatusSuccess)
}

//func (w AccountWatcher) Run() {
//	for {
//
//	}
//}
