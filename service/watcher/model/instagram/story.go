package instagram

import "story_check/ent"

type Story struct {
	Id         string `json:"id"`
	Resolution struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	} `json:"resolution"`
	Src      string `json:"src"`
	IsVideo  bool   `json:"is_video"`
	TakenAt  int64  `json:"taken_at"`
	Audience string `json:"audience"`
}

type StoryArgs struct {
	Stories []*Story
	Account *ent.Account
	Profile *ent.Profile
}
