package instagram

type Account struct {
	Id      int    `json:"id"`
	Cookies string `json:"cookies"`
}
