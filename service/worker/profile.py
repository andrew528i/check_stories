from typing import NewType

Status = NewType('ProfileStatus', str)

STATUS_UNKNOWN = Status('unknown')
STATUS_PRIVATE = Status('private')
STATUS_PUBLIC = Status('public')
STATUS_NOT_EXIST = Status('not_exist')
