from dataclasses import dataclass
import json
import re
import urllib.parse as urllib_parse
from typing import List

import requests
from selenium.common.exceptions import NoSuchElementException

from service.worker.driver import get_driver
from service.worker.account import Account
from service.worker.profile import Status, STATUS_NOT_EXIST, STATUS_UNKNOWN, STATUS_PRIVATE, STATUS_PUBLIC
from service.worker.util import search_dict

GATE = 'https://www.instagram.com'


@dataclass(init=True)
class Story:
    id: str
    resolution: dict
    src: str
    is_video: bool
    taken_at: int
    audience: str


class Client:

    @staticmethod
    def check_profile(account: Account, username: str) -> Status:
        driver = get_driver()
        account.update_driver(driver)
        profile_url = '{}/{}/'.format(GATE, username)

        driver.get(profile_url)

        try:
            driver.find_element_by_class_name("error-container")

            return STATUS_NOT_EXIST
        except NoSuchElementException:
            pass

        try:
            profile_data = driver.execute_script('return window._sharedData;')
            _, is_private = search_dict(profile_data, ['user', 'is_private'])

            if is_private:
                return STATUS_PRIVATE

            return STATUS_PUBLIC
        except:
            return STATUS_UNKNOWN

    @staticmethod
    def check_profile_story(account: Account, username: str) -> List[Story]:
        driver = get_driver()
        account.update_driver(driver)
        profile_story_url = '{}/stories/{}/'.format(GATE, username)

        driver.get(profile_story_url)
        profile_data = driver.execute_script('return window._sharedData;')
        _, profile_id = search_dict(profile_data, ['user', 'id'])  # graphql

        try:
            consumer_js_url = driver.\
                find_element_by_xpath("//link[contains(@href, 'Consumer.js')]").\
                get_property('href')
        except NoSuchElementException:
            return 'query_hash_error'

        query_hash = get_query_hash(consumer_js_url)
        if not query_hash:
            return 'no_query_hash'

        variables = {
            'reel_ids': ['{}'.format(profile_id)],
            'tag_names': [],
            'location_ids': [],
            'highlight_reel_ids': [],
            'precomposed_overlay': False,
            'show_story_viewer_list': True,
            'story_viewer_fetch_count': 50,
            'story_viewer_cursor': '',
            'stories_video_dash_manifest': False}
        encoded_variables = urllib_parse.quote(
            json.dumps(variables, separators=(',', ':')))
        stories_url = '{}/graphql/query/?query_hash={}&variables={}'.format(
            GATE, query_hash, encoded_variables)
        driver.get(stories_url)

        story_data = json.loads(driver.find_element_by_tag_name('body').text)
        _, stories = search_dict(story_data, ['reels_media', 'items'])

        if not stories:
            return []

        return [
            Story(
                s.get('id', ''),
                s.get('dimensions', {'width': 0, 'height': 0}),
                s.get(
                    'video_resources',
                    s.get('display_resources', [{'src': ''}]),
                )[-1]['src'],
                s.get('is_video', False),
                s.get('taken_at_timestamp', 0),
                s.get('audience', ''),
            ) for s in stories[::-1]
        ]


def get_query_hash(consumer_js_url: str):
    resp = requests.get(consumer_js_url)
    match = re.search(r'h="([^"]+)"', resp.text)

    if not match:
        return

    return match.group(1)
