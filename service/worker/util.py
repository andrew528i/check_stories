from copy import deepcopy


def search_dict(data, keys, result=None, current=None):
    if current is None:
        current = []

    for k, v in data.items():
        local_path = deepcopy(current)
        local_path.append(k)

        if local_path[-1 * len(keys):] == keys:
            if result is None:
                return local_path, v

            result.append(v)

        if isinstance(v, dict):
            res = search_dict(v, keys, result, local_path)
            if res and result is None:
                return res
        elif isinstance(v, list):
            for v_ in v:
                res = search_dict(v_, keys, result, local_path)
                if res and result is None:
                    return res
