import json
from dataclasses import dataclass

from selenium.webdriver.remote.webdriver import WebDriver


@dataclass(init=True)
class Account:

    id: int
    cookies: str

    def update_driver(self, driver: WebDriver):
        decoded_cookies = json.loads(self.cookies)

        for cookie in decoded_cookies:
            driver.add_cookie(cookie)
