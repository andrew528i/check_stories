from selenium import webdriver
from selenium.webdriver import DesiredCapabilities

_driver = None


def get_driver():
    global _driver

    # if _driver is None:
    capabilities = DesiredCapabilities.CHROME.copy()

    # TODO: move to config
    _driver = webdriver.Remote(
        desired_capabilities=capabilities,
        command_executor='http://0.0.0.0:4444/wd/hub')
    _driver.get('https://www.instagram.com/')

    return _driver
