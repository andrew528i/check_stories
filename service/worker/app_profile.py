import json
from dataclasses import asdict

from celery import Celery

from service.worker.account import Account
from service.worker.client import InstagramClient


app = Celery('app', broker='redis://redis:6379/1', backend='redis://redis:6379/1')
app.conf.update(
    CELERY_TASK_SERIALIZER='json',
    CELERY_ACCEPT_CONTENT=['json'],
    CELERY_RESULT_SERIALIZER='json',
    CELERY_ENABLE_UTC=True,
    CELERY_TASK_PROTOCOL=1,
)


@app.task
def instagram_check_profile(encoded_account, profile_username):
    decoded_account = json.loads(encoded_account)
    account = Account(**decoded_account)

    return InstagramClient.check_profile(account, profile_username)


if __name__ == '__main__':
    app.worker_main(['worker', '--loglevel=INFO', '--concurrency=4'])
