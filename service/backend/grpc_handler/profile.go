package grpc_handler

import (
	"context"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"story_check/ent/content"
	"story_check/ent/notification"
	"story_check/ent/profile"
	backend "story_check/service/backend/grpc_handler/pb"
)

func (h APIHandler) ProfileAdd(ctx context.Context, req *backend.ProfileAddRequest) (*empty.Empty, error) {
	u, err := h.userDao.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}

	p, err := h.profileDao.Create(req.Username, u)
	if err != nil {
		return &empty.Empty{}, err
	}

	contents, err := h.profileDao.GetLastContent(content.ContentTypeStory, p, 60*60*24)
	tx, err := h.db.Tx(ctx)
	if err != nil {
		return &empty.Empty{}, err
	}

	if contents == nil {
		return &empty.Empty{}, nil
	}

	n := tx.Notification.Create().
		SetUser(u).
		SetProfile(p).
		SetNotificationType(notification.NotificationTypeTelegram). // TODO: user settings for notification types
		SetStatus(notification.StatusQueued).
		SetUpdatedAt(time.Now())

	for _, c := range contents {
		n.AddContent(c)
	}

	_, err = n.Save(ctx)
	if err != nil {
		return &empty.Empty{}, err
	}

	if err = tx.Commit(); err != nil {
		if rErr := tx.Rollback(); rErr != nil {
			return &empty.Empty{}, err
		}

		return &empty.Empty{}, err
	}

	return &empty.Empty{}, err
}

func (h APIHandler) ProfileRemove(ctx context.Context, req *backend.ProfileRemoveRequest) (*empty.Empty, error) {
	u, err := h.userDao.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}

	_, err = h.rawDb.Query("delete from user_profiles where user_id = ? and profile_id = ?", u.ID, int(req.ProfileId))
	if err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (h APIHandler) ProfileList(ctx context.Context, _ *backend.ProfileListRequest) (*backend.ProfileListResponse, error) {
	u, err := h.userDao.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}

	ps, err := u.QueryProfiles().
		Where(
			profile.StatusIn(profile.StatusPublic, profile.StatusPrivate),
		).All(ctx)
	if err != nil {
		return nil, err
	}

	var profiles []*backend.Profile

	for _, p := range ps {
		profiles = append(profiles, &backend.Profile{
			Id:       int64(p.ID),
			Username: p.Username,
		})
	}

	return &backend.ProfileListResponse{Profiles: profiles}, nil
}
