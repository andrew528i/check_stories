package grpc_handler

import (
	"database/sql"

	"story_check/ent"
	"story_check/service/common/entity"
)

type APIHandler struct {
	userDao    entity.UserDao
	profileDao entity.ProfileDao
	db         *ent.Client
	rawDb      *sql.DB
}

func NewAPIHandler(userDao entity.UserDao, profileDao entity.ProfileDao, db *ent.Client, rawDb *sql.DB) *APIHandler {
	return &APIHandler{userDao: userDao, profileDao: profileDao, db: db, rawDb: rawDb}
}
