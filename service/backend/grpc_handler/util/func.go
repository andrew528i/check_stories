package util

import (
	"context"
	"errors"

	"google.golang.org/grpc/metadata"
)

var authErr = errors.New("unauthorized")

func GetTokenFromContext(ctx context.Context) (string, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", authErr
	}

	token, ok := md["token"]
	if !ok {
		return "", authErr
	}

	return token[0], nil
}
