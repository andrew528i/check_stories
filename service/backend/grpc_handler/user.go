package grpc_handler

import (
	"context"
	"fmt"

	"story_check/ent"
	backend "story_check/service/backend/grpc_handler/pb"
)

func (h APIHandler) UserSignIn(ctx context.Context, req *backend.UserSignInRequest) (*backend.UserSignInResponse, error) {
	var (
		user *ent.User
		err  error
	)

	switch req.Type {
	case backend.SignInType_TELEGRAM:
		user, err = h.userDao.GetOrCreateTelegram(req.TelegramChatId, req.TelegramUsername)

	case backend.SignInType_MOBILE_APP:
		panic("not implemented")

	case backend.SignInType_SITE:
		panic("not implemented")

	default:
		err = fmt.Errorf("unknown sign_in type: %d", req.Type)
	}

	if err != nil {
		return nil, err
	}

	return &backend.UserSignInResponse{Token: user.Token}, nil
}
